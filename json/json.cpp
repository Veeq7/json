#include "json.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

enum JsonErrorType {
	JsonErrorType_None = 0,
	JsonErrorType_UnexpectedToken,
};

enum JsonTokenType {
	JsonTokenType_EOF = 0,
	JsonTokenType_Number,
	JsonTokenType_String,
	JsonTokenType_OpenParen,
	JsonTokenType_CloseParen,
	JsonTokenType_OpenBracket,
	JsonTokenType_CloseBracket,
	JsonTokenType_Comma,
};

struct JsonToken {
	JsonTokenType type;
	JsonString string;
};

struct JsonParser {
	JsonRoot* root;
	char* data;
	char* cursor;
	JsonErrorType error;
};

static void* json_alloc(JsonRoot* root, size_t size);
// TODO: make `json_temp_alloc` and `json_temp_reset`

static void parser_skip_whitespace(JsonParser* parser) {
	while (*parser->cursor && isspace(*parser->cursor)) {
		++parser->cursor;
	}
}

static JsonToken get_token(JsonParser* parser) {
	if (parser->error) {
		JsonToken token = {};
		token.type = JsonTokenType_EOF;
		return token;
	}

	parser_skip_whitespace(parser);
	JsonToken token = {};
	token.string.data = parser->cursor;
	token.string.size = 1;
	switch (*parser->cursor) {
		case 0: {
			token.type = JsonTokenType_EOF;
		} break;
		case '{': {
			token.type = JsonTokenType_CloseParen;
		} break;
		case '}': {
			token.type = JsonTokenType_CloseParen;
		} break;
		case '[': {
			token.type = JsonTokenType_OpenBracket;
		} break;
		case ']': {
			token.type = JsonTokenType_CloseBracket;
		} break;
		case ',': {
			token.type = JsonTokenType_Comma;
		} break;
		case '"': {
			token.type = JsonTokenType_String;
			++parser->cursor;
			++token.string.data;
			while (*parser->cursor && *parser->cursor != '"') {
				++parser->cursor;
				++token.string.size;
			}
		} break;
		default: {
			if (isdigit(*parser->cursor) || *parser->cursor == '.') {
				token.type = JsonTokenType_Number;

				int i = 0;
				while (isdigit(*parser->cursor) || *parser->cursor == '.') {
					++parser->cursor;
					++token.string.size;
				}
				--parser->cursor;
			}
		} break;
	}
	if (token.type != JsonTokenType_EOF) {
		++parser->cursor;
	}
	return token;
}

static bool expect_token(JsonParser* parser, JsonTokenType type) {
	JsonToken token = get_token(parser);
	if (token.type == type) {
		parser->error = JsonErrorType_UnexpectedToken;
		return true;
	}
	return false;
}

static void json_parse_internal(JsonRoot* root, char* string) {
	JsonParser parser = {};
	parser.root   = root;
	parser.data   = string;
	parser.cursor = string;
}

JsonRoot json_parse(const char* string) {
	JsonRoot root = {};
	json_parse_internal(&root, (char*)string);
	return root;
}

static JsonString json_create_string_from_cstring(JsonRoot* root, const char* cstring) {
	JsonString string = {};
	string.size = strlen(cstring);
	string.data = (char*)json_alloc(root, string.size + 1);
	memcpy_s(string.data, string.size + 1, cstring, string.size);
	return string;
}

JsonRoot json_parse_copy_string(const char* string) {
	JsonRoot root = {};
	JsonString copy = json_create_string_from_cstring(&root, string);
	json_parse_internal(&root, copy.data);
	return root;
}

static JsonString json_load_entire_file(JsonRoot* root, const char* filepath) {
	FILE* file = 0;
	fopen_s(&file, filepath, "rb");

	JsonString file_data = {};
	fseek(file, 0, SEEK_END);
	file_data.size = ftell(file);
	fseek(file, 0, SEEK_SET);
	file_data.data = (char*)json_alloc(root, file_data.size + 1);
	fread(file_data.data, 1, file_data.size, file);

	fclose(file);
	return file_data;
}

JsonRoot json_parse_from_file(const char* filepath) {
	JsonRoot root = {};
	JsonString file_data = json_load_entire_file(&root, filepath);
	json_parse_internal(&root, file_data.data);
	return root;
}

static void json_free_arena(JsonMemory* memory) {
	if (memory) {
		json_free_arena(memory->next);
		free(memory);
	}
}

void json_free(JsonRoot* root) {
	json_free_arena(root->memory);
}

JsonNode* json_find(JsonNode* object, const char* key, JsonType type) {
	return 0;
}

// Utils

static void json_alloc_arena(JsonRoot* root, size_t min_size) {
	size_t size = 1024 * 1024 * 1024; // 1 MB
	if (size < min_size) {
		size = min_size;
	}
	JsonMemory* memory = (JsonMemory*)calloc(1, sizeof(JsonMemory) + size);
	if (!memory) {
		// If we couldn't allocate, the whole process of parsing will not work.
		// We should probably terminate parsing at this point, for that we need some kind of error system.
	}
	memory->next = root->memory;
	root->memory = memory;
}

static void* json_alloc(JsonRoot* root, size_t size) {
	if (!root->memory || root->memory->size + size > root->memory->capacity) {
		json_alloc_arena(root, size);
	}
	JsonMemory* memory = root->memory;
	void* data = memory->data + memory->size;
	memory->size += size;
	return data;
}