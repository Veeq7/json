#pragma once

struct JsonNode;

struct JsonNumber {
	long long integer;
	double floating;
};

struct JsonString {
	size_t size;
	char* data; // We don't null terminate, since then we don't have to reallocate the string.
};
struct JsonObject {
	size_t count;
	JsonNode* first_child;
	JsonNode* last_child;
};

struct JsonArray { // For now it is the same as JsonObject, but that might not be true in the future.
	size_t count;
	JsonNode* first_child;
	JsonNode* last_child;
};

enum JsonType {
	JsonType_Null = 0,
	JsonType_Number,
	JsonType_String,
	JsonType_Object,
	JsonType_Array,
	
	// Helpers
	JsonType_Any,
};

struct JsonNode {
	JsonNode* next;
	// Should we really have `key` here? it's needed for childs of an object, and technically every node can be one.
	// We are simplifying the code, but we are use additional 8/16 bytes where we sometimes wouldn't have to.
	JsonString key; 
	JsonType type;
	union {
		JsonNumber number;
		JsonString string;
		JsonObject object;
		JsonArray array;
	};
};

struct JsonMemory {
	JsonMemory* next;
	size_t size;
	size_t capacity;
	char data[0];
};

struct JsonRoot {
	JsonNode node; // Root node
	JsonMemory* memory;
};

JsonRoot json_parse(const char* string); // String must stay valid until JsonRoot is freed.
JsonRoot json_parse_copy_string(const char* string); // String can be invalidated anytime, since JsonRoot memory arena will allocate a copy.
JsonRoot json_parse_from_file(const char* filepath);
void json_free(JsonRoot* root);

JsonNode* json_find(JsonNode* object, const char* key, JsonType type = JsonType_Any);