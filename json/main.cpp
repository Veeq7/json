#include <stdio.h>
#include "json.h"

int main(int argc, char* argv[]) {
	JsonRoot json = json_parse_from_file("test.json");

	JsonNode* unit = json_find(&json.node, "unit", JsonType_Any);
	if (unit && unit->type == JsonType_String) {
		printf("%s", unit->string.data);
	}

	return 0;
}